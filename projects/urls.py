from django.urls import path
from projects.views import (
    project_list,
    project_list_detail,
    project_list_create,
)


urlpatterns = [
    path("", project_list, name="list_projects"),
    path("<int:id>/", project_list_detail, name="show_project"),
    path("create/", project_list_create, name="create_project"),
]
